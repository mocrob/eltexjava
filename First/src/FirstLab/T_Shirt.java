package FirstLab;

import java.util.Random;
import java.util.UUID;

public class T_Shirt extends Product {

    private static final String[] TSHIRTS_NAMES=new String[]{"red","blue","yellow","green","pink"};
    private static final String[] TSHIRTS_COMPANY_NAMES=new String[]{"KZ","Russian","GoshaRubchinsky","China"};

    public T_Shirt() {
        super();
        productID=UUID.randomUUID();
    }
    public T_Shirt(UUID id){
        super();
        productID=id;
    }
    public T_Shirt(String productName, double price, String productMakerName) {
        super(productName,price,productMakerName);
    }

    @Override
    public void create() {
        Random r=new Random();
        //this.productID=UUID.randomUUID();
        this.productName = TSHIRTS_NAMES[r.nextInt(TSHIRTS_NAMES.length)];
        this.price = 100+r.nextInt(1000)*100;
        this.productMakerName = TSHIRTS_COMPANY_NAMES[r.nextInt(TSHIRTS_COMPANY_NAMES.length)];
        productCounter++;
    }

    @Override
    public void read() {
        System.out.println("Футболка:: productID: "+productID+" Имя продукта: "+productName+" Цена:"+price+" Число товаров: "+
                productCounter+" Имя производителя: "+productMakerName);
    }


}
