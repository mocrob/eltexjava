package FirstLab;


import java.util.Random;
import java.util.UUID;

public class Cap extends Product {

    private static final String[] CAP_NAMES=new String[]{"FBI","MOSCOW","LONDON","BestCap","simple cap"};
    private static final String[] CAP_COMPANY_NAMES=new String[]{"H&M","Lacoste","GoshaRubchinsky","China"};


    public Cap() {
        super();
        productID=UUID.randomUUID();
    }
    public Cap(UUID id){
        super();
        productID=id;
    }
    public Cap(String productName, double price, String productMakerName){
        super(productName,price,productMakerName);
    }

    public void create() {
        Random r=new Random();
        // this.productID = r.nextInt(1000);
        this.productID=UUID.randomUUID();
        this.productName = CAP_NAMES[r.nextInt(CAP_NAMES.length)];
        this.price = 100+r.nextInt(1000)*100;
        this.productMakerName = CAP_COMPANY_NAMES[r.nextInt(CAP_COMPANY_NAMES.length)];
        productCounter++;
    }


    public void read() {
        System.out.println("Кепка:: productID: "+productID+" Имя продукта: "+productName+" Цена:"+price+" Число товаров: "+
                productCounter+" Имя производителя: "+productMakerName);

    }

}
