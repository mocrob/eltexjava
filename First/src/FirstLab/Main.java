package FirstLab;

public class Main {

    public static void main(String[] args){
        int objCount =Integer.parseInt(args[0]);
        String type =args[1];
        Product[] products=new Product[objCount];
        if(type.toLowerCase().equals("cap")){
        for(int i=0;i<objCount;i++){
            products[i]=new Cap();
            products[i].create();
            products[i].read();
           //products[i].delete();
            //products[i].read();
        }
        }
        else{
            if(type.toLowerCase().equals("t-shirt")){
                for(int i=0;i<objCount;i++){
                    products[i]=new T_Shirt();
                    products[i].update();
                    products[i].read();
                }
            }
            else return;
        }
    }
}
